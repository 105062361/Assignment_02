# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

* Student ID : 105062361
* Name : 伍瀚翔

## Scoring 
|                                              Item                                              | Score | Y/N |
|:----------------------------------------------------------------------------------------------:|:-----:|:--:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  | Y |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  | Y |
|         All things in your game should have correct physical properties and behaviors.         |  15%  | Y |
| Set up some interesting traps or special mechanisms.(at least 2 different kinds of platform) |  10%  | Y |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  | Y |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  | Y |
| Appearance (subjective)                                                                        |  10%  | Y |
| Other creative features in your game (describe on README.md)                                   |  10%  | Y |

* 由於 Gitlab page 極度不穩定，在此附上firebase hosting的版本：
=> https://assignment2-1f54c.firebaseapp.com

## Additional description

1. Five state complete game process: Boot state, Load state, Leaderboard state, Play state, Game Over state.
2. Interesting traps or special mechanisms: normal platform, conveyor left platform and conveyor right platform (Total three platforms).
3. Additional sound effects: Menu music, Leaderboard music, Play game music, Player die music and Game over music.
4. Additional UI: Progress Bar, Play or Pause button, Scoring Text
5. Leaderboard: Top three highest score player (Scoring based on money bag the player take in the game)
6. Other creative features: Game Pause and resume, Money bag system (take one money bag for 100 score)