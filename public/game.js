var game = new Phaser.Game(600, 400, Phaser.AUTO, "canvas");
//define global variable
game.global = {score: 0, life_num: 15, floor_num: 0};
//add all states
game.state.add("boot", bootState);
game.state.add("load", loadState);
game.state.add("menu", menuState);
game.state.add("leaderboard", leaderboardState);
game.state.add("play", playState);
game.state.add("gameOver", gameOverState);
game.state.start("boot");
