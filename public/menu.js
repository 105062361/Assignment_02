var menuMusic;
var endingMusic;
var leaderboardMusic;

var menuState = {
    create: function(){
        game.add.image(0, 0, "menuBackground");

        var nameLabel = game.add.text(game.width/2, 100, "NS-Shaft", {font: "55px Bungee Shade Arial", fill: "#000000"});
        nameLabel.anchor.setTo(0.5, 0.5);

        var leaderboardBtn = game.add.button(game.width/2, game.height/2 + 40, "button", leaderboardClick, this);
        leaderboardBtn.anchor.setTo(0.5, 0.5);
        var leaderboardText = game.add.text(0, 0, "Leaderboard", {font: "16px Arial", fill: "#000000"});
        leaderboardText.anchor.setTo(0.5, 0.5);
        leaderboardBtn.addChild(leaderboardText);
        leaderboardBtn.input.useHandCursor = true;

        var startBtn = game.add.button(game.width/2, game.height-80, "button", playClick, this);
        startBtn.anchor.setTo(0.5, 0.5);
        var startText = game.add.text(0, 0, "Play", {font: "16px Arial", fill: "#000000"});
        startText.anchor.setTo(0.5, 0.5);
        startBtn.addChild(startText);
        startBtn.input.useHandCursor = true;
        
        
        menuMusic = game.add.audio("menu");
        menuMusic.loop = true;
        menuMusic.play();

        endingMusic = game.add.audio("ending");
        endingMusic.loop = true;

        leaderboardMusic = game.add.audio("leaderboard");
        leaderboardMusic.loop = true;
        


        
    }
}

function leaderboardClick(){
    game.state.start("leaderboard");
}

function playClick(){
    game.state.start("play");
}