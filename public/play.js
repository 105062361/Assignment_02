var cursor;
var player;
var platforms = [];
var leftWall, rightWall, ceiling;
var life, score, floor;//label
// var floorNum = 0;//int
var lastTime = 0;
var unbeatableTime = 0;
var bgMusic;
var money = [];

var playState = {
    preload: function(){

    },

    create: function(){
        game.add.image(0, 0, "backgroundImage");
        game.global.score = 0;
        game.global.life_num = 15;
        game.global.floor_num = -1;
        cursor = game.input.keyboard.createCursorKeys();
        createBounders();
        createPlayer();
        createTextBar();

        menuMusic.stop();
        bgMusic = game.add.audio("bg_music");
        bgMusic.loop = true;
        bgMusic.play();

        
        
    },

    update: function(){
        
        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        checkTouchCeiling(player);
        checkGameOver();

        createPlatforms();

        updatePlayer();
        updatePlatforms();
        updateTextBar();

    }
}

function createBounders(){
    leftWall = game.add.sprite(0, 0, "wall");
    rightWall = game.add.sprite(482, -100, "wall");
    ceiling = game.add.sprite(0, 0, "ceiling");
    ceiling2 = game.add.sprite(400, 0, "ceiling2");

    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable= true;

    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    game.physics.arcade.enable(ceiling);
    game.physics.arcade.enable(ceiling2);
}

function createPlayer(){
    player = game.add.sprite(game.width/2, 30, "player");
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    //player.life = 15;
    player.onTouch = undefined;
    //  8 is the frame rate (8fps)
    //  true means it will loop when it finishes
    player.animations.add('left', [0, 1, 2, 3], 8, true);
    player.animations.add('right', [9, 10, 11, 12], 8, true);
    player.animations.add('flyLeft', [18, 19, 20, 21], 12, true);
    player.animations.add('flyRight', [27, 28, 29, 30], 12, true);
    player.animations.add('fly', [36, 37, 38, 39], 12, true);
}

function createTextBar(){
    life = game.add.text(510, 10, "Life: -1", {fill: "#000000", fontSize: "18px"});
    score = game.add.text(510, 60, "Score: -1", {fill: "#000000", fontSize: "18px"});
    floor = game.add.text(510, 110, "Floor: -1", {fill: "#000000", fontSize: "18px"});
    
    // Create a label to use as a button
    var pauseBtn = game.add.button(550, 180, "button", pauseClick, this);
    pauseBtn.anchor.setTo(0.5, 0.5);
    pauseBtn.scale.setTo(0.5, 0.5);
    var pauseText = game.add.text(0, 0, "Pause", {font: "25px Arial", fill: "#000000"});
    pauseText.anchor.setTo(0.5, 0.5);
    pauseBtn.addChild(pauseText);
    pauseBtn.input.useHandCursor = true;

    var playBtn = game.add.button(550, 230, "button", playGame, this);
    playBtn.anchor.setTo(0.5, 0.5);
    playBtn.scale.setTo(0.5, 0.5);
    var playText = game.add.text(0, 0, "Play", {font: "25px Arial", fill: "#000000"});
    playText.anchor.setTo(0.5, 0.5);
    playBtn.addChild(playText);
    playBtn.input.useHandCursor = true;
    
}


function checkTouchCeiling(player)
{
    if(player.body.y < 0)
    {
        if(game.time.now > unbeatableTime)
        {
            game.global.life_num -= 5;
            game.camera.flash(0xff0000, 200);
            unbeatableTime = game.time.now + 500;
        }
    }
}

function checkGameOver()
{
    if(game.global.life_num <= 0 || player.body.y > 500)
    {
        gameOver();
    }
}

function gameOver()
{
    for(var i = 0; i < platforms.length; i++)
    {
        platforms[i].destroy();
    }
    platforms = [];

    for(var j = 0; j < money.length; j++)
    {
        money[j].destroy();
    }
    money = [];
    

    dieMusic = game.add.audio("die");
    dieMusic.play();
    game.time.events.add(500, function(){
        game.global.life_num = -1;
        dieMusic.stop();
        game.state.start("gameOver");
    });
    
}

function updatePlayer(){
    if(cursor.left.isDown)
    {
        player.body.velocity.x = -250;
    }
    else if(cursor.right.isDown)
    {
        player.body.velocity.x = 250;
    }
    else
    {
        player.body.velocity.x = 0;
        
    }
    setPlayerAnimation(player);
}

function setPlayerAnimation(player){
    var velocityX = player.body.velocity.x;
    var velocityY = player.body.velocity.y;

    if(velocityX < 0 && velocityY == 0)
    {
        player.animations.play("left");
    }
    else if(velocityX > 0 && velocityY == 0)
    {
        player.animations.play("right");
    }
    else if(velocityX < 0 && velocityY > 0)
    {
        player.animations.play("flyLeft");
    }
    else if(velocityX > 0 && velocityY > 0)
    {
        player.animations.play("flyRight");
    }
    else if(velocityX == 0 && velocityY != 0)
    {
        player.animations.play("fly");
    }
    else
    {
        player.frame = 8;
    }
}

function updatePlatforms(){
    for(var i = 0; i < platforms.length; i++)
    {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -10)
        {
            platform.destroy();
            platforms.splice(i, 1);//Remove 1 element from index i 
        }
    }

    

    for(var j = 0; j < money.length; j++)
    {
        var oneMoney = money[j];
        oneMoney.body.position.y -= 2;
        if(oneMoney.body.position.y <= -10)
        {
            oneMoney.destroy();
            money.splice(j, 1);
        }
        game.physics.arcade.overlap(player, oneMoney, takeMoney, null, this);

    }
}

function updateTextBar(){
    life.setText("Life: " + game.global.life_num);
    score.setText("Score: " + game.global.score);
    floor.setText("Floor: B" + game.global.floor_num);
}

function createPlatforms(){
    if(game.time.now > lastTime + 500){
        lastTime = game.time.now;
        createOnePlatform();
        game.global.floor_num++;
    }
}

function createOnePlatform(){
    var platform;
    var oneMoney;
    var x = (Math.random() * (500 - 2 * 20 - 96) ) + 18;//avoid create platform at leftWall or rightWall
    var y = 400;
    var ranNum = Math.random() * 80;//for creating different platform

    if(ranNum < 20)
    {
        platform = game.add.sprite(x, y, "nails");
    }
    else if(ranNum < 40)
    {
        platform = game.add.sprite(x, y, "conveyorLeft");
        platform.animations.add('scrollLeft', [0, 1, 2, 3], 20, true);
        platform.play('scrollLeft');
    }
    else if(ranNum < 60)
    {
        platform = game.add.sprite(x, y, "conveyorRight");
        platform.animations.add('scrollRight', [0, 1, 2, 3], 20, true);
        platform.play('scrollRight');
    }
    else
    {
        platform = game.add.sprite(x, y, "normal");//(x, y, key)
        oneMoney =  game.add.sprite(x + ranNum, y - 20, "money");
        oneMoney.scale.setTo(0.1, 0.1);
        game.physics.arcade.enable(oneMoney);
        money.push(oneMoney);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true; 
    platforms.push(platform);

    //money
    
    // if(ranNum > 20 && ranNum < 40)
    // {
    //     oneMoney =  game.add.sprite(x, y, "money");
    //     oneMoney.scale.setTo(0.5, 0.5);
    // }
    // else if(ranNum > 70 && ranNum < 90)
    // {
    //     oneMoney =  game.add.sprite(x, y, "money");
    //     oneMoney.scale.setTo(0.5, 0.5);
    // }

    
}

function effect(player, platform){
    if(platform.key == 'normal')
    {
        if (player.onTouch != platform) //to avoid the player life increase >= 1 time(s) in the update function, make sure that to add life once in this function
        {
            if(game.global.life_num < 15)
            {
                game.global.life_num++;
            }
            player.onTouch = platform;
        }
    }
    else if(platform.key == 'nails')
    {
        if(player.onTouch != platform)
        {
            game.global.life_num -= 5;
            if(game.global.life_num <= 0)
            {
                game.global.life_num = 0;
            }
            game.camera.flash(0xff0000, 200);
            player.onTouch = platform;
        }
        
    }
    else if(platform.key == "conveyorLeft")
    {
        player.body.x -= 3;
    }
    else//conveyorRight
    {
        player.body.x += 3;
    }
}

function pauseClick(){
    game.paused = true;
}

function playGame(){
    game.paused = false;
}

function takeMoney(player, money){
    money.kill();
    game.global.score += 100;
}


