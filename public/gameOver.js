var gameOverState = {
    create: function(){
        game.add.image(0, 0, "menuBackground");

        var nameLabel = game.add.text(game.width/2, 80, "Game Over", {font: "50px Bungee Arial", fill: "#000000"});
        nameLabel.anchor.setTo(0.5, 0.5);

        var scoreLabel = game.add.text(game.width/2, game.height/2 - 30, "Score: " + game.global.score, {font: "20px Bungee", fill: "#54524a"});
        scoreLabel.anchor.setTo(0.5, 0.5);

        var floorLabel = game.add.text(game.width/2, game.height/2, "Floor: B" + game.global.floor_num, {font: "20px Bungee", fill: "#54524a"});
        floorLabel.anchor.setTo(0.5, 0.5);

        var menuBtn = game.add.button(game.width/2, game.height/2 + 80, "button", menuClick, this);
        menuBtn.anchor.setTo(0.5, 0.5);
        var menuText = game.add.text(0, 0, "Menu", {font: "16px Arial", fill: "#000000"});
        menuText.anchor.setTo(0.5, 0.5);
        menuBtn.addChild(menuText);
        menuBtn.input.useHandCursor = true;

        var startBtn = game.add.button(game.width/2, game.height - 60, "button", playClick, this);
        startBtn.anchor.setTo(0.5, 0.5);
        var startText = game.add.text(0, 0, "Play Again", {font: "16px Arial", fill: "#000000"});
        startText.anchor.setTo(0.5, 0.5);
        startBtn.addChild(startText);
        startBtn.input.useHandCursor = true;

        var name = prompt("Please enter your name");
        if(name) {    
            console.log("Hello "+ name +", nice to meet you!");
            var databaseRef =  firebase.database().ref("leaderboard/" + name);
            databaseRef.set({
                name: name,
                score: game.global.score,
                floor: game.global.floor_num
            });

        }

        bgMusic.stop();
        endingMusic.play();
    }
}

function menuClick(){
    endingMusic.stop();
    game.state.start("menu");
}

function playClick(){
    endingMusic.stop();
    game.state.start("play");
}