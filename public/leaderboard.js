var leaderboardState ={
    preload: function(){

    },

    create: function(){
        game.add.image(0, 0, "menuBackground");
        var databaseRef =  firebase.database().ref("leaderboard");
        var top3_name = [];
        var top3_score = [];
        // var top3_floor = [];

        databaseRef.orderByChild("score").limitToLast(3).once("value").then(function(snapshot){
            snapshot.forEach(childSnapshot => {
                var childData = childSnapshot.val();
                console.log(childData.name + " " + childData.score + " " + childData.floor);
                top3_name.unshift(childData.name);
                top3_score.unshift(childData.score);
                // top3_floor.unshift(childData.floor);
            });
            var playerTitle = game.add.text(180, 150, "Player Name" , {font: "25px Bungee Arial", fill: "#000000"});
            playerTitle.anchor.setTo(0, 0.5);

            var scoreTitle = game.add.text(360, 150, "Score" , {font: "25px Bungee Arial", fill: "#000000"});
            scoreTitle.anchor.setTo(0, 0.5);

            for(var i = 0, j = 100; i < top3_name.length; i++, j += 50)
            {
                var playerLabel = game.add.text(180, 100 + j, top3_name[i] , {font: "25px Bungee Arial", fill: "#000000"});
                playerLabel.anchor.setTo(0, 0.5);

                var scoreLabel = game.add.text(360, 100 + j, top3_score[i] , {font: "25px Bungee Arial", fill: "#000000"});
                scoreLabel.anchor.setTo(0, 0.5);
            }
            
        });

        var nameLabel = game.add.text(game.width/2, 50, "Top 3 Highest Scoring Player", {font: "40px Bungee Arial", fill: "#ff0000"});
        nameLabel.anchor.setTo(0.5, 0.5);

        var backBtn = game.add.button(game.width - 95, game.height - 20, "button", backClick, this);
        backBtn.anchor.setTo(0.5, 0.5);
        var backText = game.add.text(0, 0, "Back To Menu", {font: "16px Arial", fill: "#000000"});
        backText.anchor.setTo(0.5, 0.5);
        backBtn.addChild(backText);
        backBtn.input.useHandCursor = true;

        menuMusic.stop();
        leaderboardMusic.play();
    },

    update: function(){

    }
}

function backClick(){
    leaderboardMusic.stop();
    game.state.start("menu");
}