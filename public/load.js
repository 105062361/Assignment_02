var endingMusic;

var loadState = {
    preload: function(){
        //loadingLabel
        var loadingLabel = game.add.text(game.width/2, 150, "Loading...", {font: "45px Arial", fill: "#ffffff"});
        loadingLabel.anchor.setTo(0.5, 0.5);

        //progressBar
        var progressBar = game.add.sprite(game.width/2, 250, "progressBar");
        progressBar.anchor.set(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);

        //load all game assets
        game.load.image("backgroundImage", "assets/bg.png");
        game.load.image("ceiling", "assets/ceiling.png");
        game.load.image("ceiling2", "assets/ceiling2.png");
        game.load.image("nails", "assets/nails.png");
        game.load.image("normal", "assets/normal.png");
        game.load.image("wall", "assets/wall2.png");
        game.load.image("money", "assets/money.png");
        game.load.spritesheet("player", "assets/player.png", 32, 32);
        game.load.spritesheet("conveyorLeft", "assets/conveyor_left.png", 96, 16);
        game.load.spritesheet("conveyorRight", "assets/conveyor_right.png", 96, 16);

        //load a asset that use in menu state
        game.load.image("menuBackground", "assets/menu_bg.png");
        game.load.image("button", "assets/yellowBtn.png");

        game.load.audio("die", "assets/die.wav");
        game.load.audio("ending", "assets/ending.wav");
        game.load.audio("menu", "assets/menu.wav");
        game.load.audio("bg_music", "assets/bg_music.wav");
        game.load.audio("leaderboard", "assets/leaderboard.wav");
    },

    create: function(){
        game.state.start("menu");
    }
}